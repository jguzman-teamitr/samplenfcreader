//
//  ViewController.swift
//  SampleNFCReader
//
//  Created by Jacquiline Guzman on 2019/11/24.
//  Copyright © 2019 teamimpact. All rights reserved.
//

import UIKit
import CoreNFC

class ViewController: UIViewController {

    @IBOutlet weak var messageTextView: UITextView!
    var nfcSession: NFCNDEFReaderSession?
    var nfcFelicaSession: NFCTagReaderSession?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func scanPressed(_ sender: Any) {
        print("The scan button has been pressed. Initializing the nfc session")

        nfcSession = NFCNDEFReaderSession.init(
            delegate: self,
            queue: nil,
            invalidateAfterFirstRead: true)
        nfcSession?.begin()
    }

    @IBAction func scanFelicaPressed(_ sender: Any) {
        print("The felica variant has been selected. Will be initializing the nfc session")

        nfcFelicaSession = NFCTagReaderSession(pollingOption: .iso18092, delegate: self)
        guard let session = nfcFelicaSession else {
            print("Felica session is null.")
            return
        }

        session.alertMessage = "Hold your iPhone near the tag or card"
        session.begin()
    }
}

extension ViewController: NFCNDEFReaderSessionDelegate {
    func readerSessionDidBecomeActive(_ session: NFCNDEFReaderSession) {
        print("Reader session did become active")
    }

    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("The session was invalidated: \(error.localizedDescription)")
    }

    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        var result = ""
        for payload in messages[0].records {
           result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)!
        }

        DispatchQueue.main.async {
            self.messageTextView.text = result
        }
    }
}

extension ViewController: NFCTagReaderSessionDelegate {
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print("FeliCa session did become active.")
    }

    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print("The felica session was invalidated: \(error.localizedDescription)")
    }

    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        print("did detect some tags...")
        
        guard let tag = tags.first else {
            print("Unable to get the tag.")
            return
        }

        session.connect(to: tag) { (error) in
            if nil != error {
                print("Error: ", error.debugDescription)
                return
            }
            guard case .feliCa(let feliCaTag) = tag else {
                session.alertMessage = "A non-FeliCa tag has been detected. Please try again within another card."
                return
            }

            let idm = feliCaTag.currentIDm.map { String(format: "%.2hhx", $0) }.joined()
            let systemCode = feliCaTag.currentSystemCode.map { String(format: "%.2hhx", $0) }.joined()

            let serviceCode = Data([0x09,0x0f].reversed())

            feliCaTag.requestService(nodeCodeList: [serviceCode]) { nodes, error in
                if let error = error {
                    print("Error:", error)
                    return
                }

                guard let data = nodes.first, data != Data([0xff, 0xff]) else {
                    print("No historical information available")
                    return
                }

                let block:[Data] = (0..<12).map { Data([0x80, UInt8($0)]) }
                feliCaTag.readWithoutEncryption(serviceCodeList: [serviceCode], blockList: block) {status1, status2, dataList, error in

                    if let error = error {
                        print("Error: ", error)
                        return
                    }

                    guard status1 == 0x00, status2 == 0x00 else {
                        print("Invalid status code: ", status1, " / ", status2)
                        return
                    }

                    session.invalidate()

                    print("IDm: \(idm)")
                    print("System Code: \(systemCode)")

                    dataList.forEach { data in
                        print("Year: ", Int(data[4] >> 1) + 2000)
                        print("Month: ", ((data[4] & 1) == 1 ? 8 : 0) + Int(data[5] >> 5))
                        print("Day: ", Int(data[5] & 0x1f))
                        print("Code of entry station (train): ", data[6...7].map { String(format: "%02x", $0) }.joined())
                        print("Code of exit station (train): ", data[8...9].map { String(format: "%02x", $0) }.joined())
                        print("Remaining balance (JPY): ", Int(data[10]) + Int(data[11]) << 8)
                        print("==================================================")
                    }
                }
            }
        }
    }
}
